# Python Clean Architecture Demo

Based on [The Clean Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html) by Rober Martin.

## Prerequisites

* Python 3.9

## Installation

1. Install [Poetry](https://python-poetry.org/)
2. Install Python dependencies: `poetry install`
3. Copy example.env to .env and update values as you see fit

## Run the app

### VS Code

1. Select the Python interpreter. Should be something like `~/.cache/pypoetry/virtualenvs/example-MIOzMQhO-py3.0/bin/python`
2. Press F5 or Debug: Start Debugging from the command pallete.

### Command Line

1. `poetry shell`
2. `export FLASK_APP=example.app.py`
3. `flask run --reload`
