from flask import Flask
from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()


def create_app():
    """
    Construct the core application
    """
    app = Flask(__name__, instance_relative_config=False)
    app.config.from_object("config.Config")

    db.init_app(app)

    with app.app_context():
        from . import routes
        from example.repos import UserRepo
        from example.services import UserService

        db.create_all()
        user_repo = UserRepo(db)
        user_service = UserService(repo=user_repo)
        app.services = {
            'user': user_service
        }

        return app
