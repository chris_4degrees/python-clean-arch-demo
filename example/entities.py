import re


class InvalidEmail(Exception):
    pass


class InvalidUsername(Exception):
    pass


class User:
    """
    User domain entity
    """

    def __init__(self, username: str, email: str, bio: str = None, admin: bool = False):
        self.username = username
        self.email = email
        self.bio = bio
        self._admin_role = admin

    def __repr__(self):
        return f"<User {self.username}>"

    @property
    def is_admin(self):
        return self._admin_role and self.email.endswith("4degrees.ai")

    def to_dict(self):
        return {
            "username": self.username,
            "email": self.email,
            "bio": self.bio,
            "admin": self.is_admin,
        }
