import abc
import example.entities as entities
import example.orm as orm


class AbstractUserRepo(abc.ABC):
    """
    Abstract class for user repositories. Gives us a consistent interface we can depend on
    when using dependency injection
    """
    @abc.abstractmethod
    def add(self, user: entities.User):
        """
        Add a user
        """
        raise NotImplementedError

    @abc.abstractmethod
    def get(self, user_id: int) -> entities.User:
        """
        Add a user by id
        """
        raise NotImplementedError

    @abc.abstractmethod
    def list(self) -> list[entities.User]:
        """
        List all users
        """
        raise NotImplementedError

    @abc.abstractmethod
    def _from_orm(orm_model) -> entities.User:
        raise NotImplementedError

    @abc.abstractmethod
    def _to_orm(user: entities.User):
        raise NotImplementedError


class UserRepo(AbstractUserRepo):
    """
    Concrete instance of our user repository. Abstracts data access and storage for users so our
    services don't need to know about database implementation details.
    """
    def __init__(self, db):
        self.db = db

    def add(self, user: entities.User):
        orm_user = self._to_orm(user)
        self.db.session.add(orm_user)
        self.db.session.commit()

    def get(self, user_id: int) -> entities.User:
        orm_user = orm.User.query.filter_by(id=user_id).first()

        return self._from_orm(orm_user)

    def list(self) -> list[entities.User]:
        orm_users = orm.User.query.all()
        users = [self._from_orm(orm_user) for orm_user in orm_users]

        return users

    def _from_orm(self, orm_model) -> entities.User:
        return entities.User(
            username=orm_model.username,
            email=orm_model.email,
            bio=orm_model.bio,
            admin=orm_model.admin,
        )

    def _to_orm(self, user: entities.User):
        return orm.User(
            username=user.username, email=user.email, bio=user.bio, admin=user.admin
        )
