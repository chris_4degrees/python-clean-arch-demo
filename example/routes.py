from flask import current_app as app
from flask import request, jsonify

import example.entities as entities


@app.route("/user/<user_id>", methods=["GET"])
def retrieve_user(user_id):
    user_id = int(user_id)
    user = app.repo.get(user_id)

    return jsonify(
        {
            "username": user.username,
            "email": user.email,
            "bio": user.bio,
            "admin": user.admin,
        }
    )

@app.route("/users", methods=["GET"])
def list_users():
    service = app.services.get("user")
    repo = service.repo
    users = repo.list()
    user_data = [u.to_dict() for u in users]

    return jsonify({"status": "success", "data": user_data})


@app.route("/users", methods=["POST"])
def create_user():
    service = app.services.get("user")
    if service is None:
        return jsonify({"status": "error", "message": "No User Service found"}), 500

    request_data = request.get_json()

    try:
        service.create_user(**request_data)

        return jsonify({"status": "success", "msg": None}), 201

    except (entities.InvalidUsername, entities.InvalidEmail) as e:
        return jsonify({"status": "error", "msg": str(e)}), 400
