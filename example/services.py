import re

import example.entities as entities
import example.repos as repos


class UserService:
    """
    This is our main entrypoint for running commands on our user domain entity
    """
    def __init__(self, repo: repos.AbstractUserRepo):
        """
        Uses dependency injection to provide a concrete implementation of our UserRepo interface
        to allow us to override our dependencies during testing
        """
        self.repo = repo

    def create_user(
        self, username: str, email: str, bio: str = None, admin=False
    ) -> entities.User:
        """
        Create a user. Performs validation and leverages our UserRepo
        """
        # Validators
        def _validate_email(email: str) -> str:
            regex = r"\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b"

            if not re.fullmatch(regex, email):
                raise entities.InvalidEmail("Invalid email address")

            return email

        def _validate_username(username: str) -> str:
            regex = r"^[a-z_]([a-z0-9_-]{0,31}|[a-z0-9_-]{0,30}\$)$"

            if not re.fullmatch(regex, username):
                raise entities.InvalidEmail("Invalid username")

            return username

        # Create user entity
        user = entities.User(
            username=_validate_username(username),
            email=_validate_email(email),
            bio=bio,
            admin=admin,
        )

        # Save user to repository
        self.repo.add(user)

        return user
