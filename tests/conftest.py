import pytest
from flask import Flask

import example.repos as repos
import example.entities as entities
import example.services as services


class FakeUserRepo(repos.AbstractUserRepo):
    """
    A fake version of our user repository that implements the methods
    of our AbstractUserRepo without using a real database
    """
    def __init__(self):
        self.users = {}
        self.id_counter = 0

    def add(self, user: entities.User):
        next_id = self.id_counter + 1
        self.id_counter += 1
        user.id = next_id
        self.users[next_id] = user

    def get(self, user_id: int) -> entities.User:
        return self.users.get(user_id)

    def list(self) -> list[entities.User]:
        return list(self.users.values())

    def _from_orm(orm_model) -> entities.User:
        pass

    def _to_orm(user: entities.User):
        pass


@pytest.fixture()
def user_service():
    """
    A test version of our UserService that leverages our FakeUserRepo
    """
    user_repo = FakeUserRepo()

    return services.UserService(repo=user_repo)


@pytest.fixture(scope="module")
def app():
    """
    A test version of our Flask application
    """
    app = Flask(__name__, instance_relative_config=False)
    app.config["TESTING"] = True
    app.config["SECRET_KEY"] = "TEST_SECRET_KEY"
    app.config["FLASK_ENV"] = "test"

    with app.app_context():
        from example import routes
        from example.services import UserService

        user_repo = FakeUserRepo()
        user_service = UserService(repo=user_repo)
        app.services = {"user": user_service}

        return app


@pytest.fixture(scope="module")
def client(app):
    """
    Our test Flask client that we can user for API/Functional tests
    """
    with app.test_client() as client:
        with app.app_context():
            yield client
