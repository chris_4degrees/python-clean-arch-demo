import pytest

import example.entities as entities


###################################################################################
# Unit Tests
###################################################################################

# Service Tests
def test_create_user_with_valid_email(user_service):
    try:
        user_service.create_user(username="test_user", email="test_user@example.com")
    except entities.InvalidEmail as e:
        assert False, f"Creating user raised an exception: {e}"


def test_create_user_with_invalid_email(user_service):
    with pytest.raises(entities.InvalidEmail):
        user_service.create_user(username="test_user", email="bad_email_address")

def test_create_user_with_valid_username(user_service):
    try:
        user_service.create_user(username="test_user", email="test_user@example.com")
    except entities.InvalidUsername as e:
        assert False, f"Creating user raised an exception: {e}"

def test_create_user_with_invalid_username(user_service):
    with pytest.raises(entities.InvalidEmail):
        user_service.create_user(username="This is a bad username", email="test_user@example.com")

# Domain/Entity tests
def test_non_4deg_user_is_not_admin():
    user = entities.User(
        username="test_user", email="test_user@example.com", admin=True
    )

    assert not user.is_admin


def test_4deg_user_with_admin_role_is_admin():
    user = entities.User(
        username="test_user", email="test_user@4degrees.ai", admin=True
    )

    assert user.is_admin


###################################################################################
# API Tests
###################################################################################
def test_post_user_returns_201(client):
    response = client.post(
        "/users", json={"username": "test_user", "email": "test_user@example.com"}
    )

    assert response.status_code == 201


def test_post_user_returns_201(client):
    response = client.post(
        "/users", json={"username": "test_user", "email": "test_user@example.com"}
    )

    assert response.status_code == 201


def test_post_user_with_invalid_email_returns_400(client):
    response = client.post(
        "/users", json={"username": "test_user", "email": "bad_email_address"}
    )

    assert response.status_code == 400


def test_post_user_with_invalid_username_returns_400(client):
    response = client.post(
        "/users", json={"username": "a bad username", "email": "test_user@example.com"}
    )

    assert response.status_code == 400


def test_list_users_returns_200(client):
    response = client.get("/users")

    assert response.status_code == 200
